### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ a4f98e83-cb95-4242-9f67-8a387bef8eb9
using Pkg

# ╔═╡ 9b206d20-caf3-11ec-0206-bb60a1af7ad5
using Markdown

# ╔═╡ 8057b77b-9925-4931-b783-9804e283085b
using InteractiveUtils

# ╔═╡ 8b3485aa-e839-4272-96af-b7c282336b69
using DataStructures

# ╔═╡ 52bf76b6-5106-44dc-b212-79cf54663adc
using PlutoUI

# ╔═╡ 113b0c9d-2bc8-4dc0-8b91-39af696fa47d
md"
# ARI711S - Assignment 2
"


# ╔═╡ 0540ab39-fd77-4325-90db-e6c6db15a359
md"
# Problem 1
"


# ╔═╡ d2a3ad4b-c0a1-4f84-a050-5349f8551c20
md"""Asessment Criteria

* correct implementation of the algorithms;
* quality of the solution;
* handling of input at runtime;
* the overall solution, as well as the management of the Julia environment.




"""


# ╔═╡ f4e917cd-8352-403e-beb8-5b95ed66aab5
md"
# User Configurations
"

# ╔═╡ a0b9597a-bb37-478d-9943-7e22d894f900
mutable struct Position
	F::Int
	O::Int
end


# ╔═╡ 7905b5cf-47ec-4fa9-a7b4-099181cda229
struct State
	parcels::Matrix{Int}
	Agent::Position
end


# ╔═╡ fc61dbec-0acd-4940-9cff-341f3b0d4191
struct Action
	name::String
	cost::Int64
end


# ╔═╡ 6b5017a7-6c0b-432f-a669-1de5c4f8c5e2
md"#### Building Structure :"

# ╔═╡ b2ace667-ba45-4019-b33d-b8e979f9b747
md"#### Select Number of Offices"

# ╔═╡ 50b7aee0-fe18-43bc-8986-1be8adcc0b47
begin
	@bind office1 TextField()
	
end


# ╔═╡ f9771065-d0ba-4687-a141-1870b93b2173
num1= parse(Int8, office1)

# ╔═╡ 10d38fe0-60d1-4ada-bb5d-241eba53f3aa
md"#### Select Number of Floors"

# ╔═╡ 6bcfb880-cc86-4dd2-8769-ee5fc2e23ca7
begin
	@bind floors TextField()
end

# ╔═╡ ebbd3aa4-9550-472d-93af-12a369879302
num2= parse(Int8, floors)

# ╔═╡ 923af068-39e6-4ff0-aeb4-6ac1ca433101
function build(office,floors)
	parcels = zeros(Int8, office, floors)
	parcels[1,1] = 2
	parcels[1,3] = 1
	parcels[2,3] = 2
	State_1 = State(parcels, Position(1,1))
	# Goal_State=0
	# # println("Building Multi storey")
	# # println("============================")
	# # @show State_1
	# # @show Goal_State
	# # println("============================")
end

# ╔═╡ 1a0641ad-d75b-4d95-a574-a68c87a0fd80
X=build(num1,num2)

# ╔═╡ 8928d519-079b-44e7-888d-7ded0c5d7941
md"#Differnet Transitions
"

# ╔═╡ f787be50-eb2c-48cd-ac34-38f4b552bd0b
begin

		parcels = zeros(Int8, num1, num2)
		parcels[1,1] = 2
		parcels[1,3] = 1
		parcels[2,3] = 2
	
	    parcels2 = zeros(Int8, num1, num2)
		parcels2[1,3] = 1
		parcels2[2,3] = 2

	    parcels3= zeros(Int8, num1, num2)
		parcels3[2,3] = 2


	    parcels4= zeros(Int8, num1, num2)
	
		State_2 = State(parcels, Position(1,2))
	    State_3 = State(parcels, Position(1,3))
	    State_4 = State(parcels, Position(2,1))
	    State_5 = State(parcels, Position(2,1))
		State_6 = State(parcels, Position(2,2))
	    State_7 = State(parcels, Position(2,3))
	    State_8 = State(parcels, Position(3,1))
	    State_9 = State(parcels, Position(3,2))
	    State_10 = State(parcels, Position(3,3))
	    State_11 = State(parcels2, Position(1,1))
	    State_12 = State(parcels3, Position(2,3))
	    State_13 = State(parcels4, Position(1,3))

end


# ╔═╡ 821f4235-0d9b-429d-9148-09217b15e51e
md"#### Define the actions"

# ╔═╡ ad934ec1-d89c-4066-b535-cc7e37f2e32f
begin
	mu  = Action("MOVE WEST", 1)
	md = Action("MOVE WEST", 1)
	mw= Action("MOVE WEST", 2)
	me = Action("MOVE EAST", 2)
	r  = Action("REMAIN",0)
	co  = Action("COLLECT", 5)	
end



# ╔═╡ 51282cb4-22f4-4240-b9a1-b3e7f92359fc
begin
	Transitions = Dict()
	push!(Transitions,X => [(me, State_2), (md, State_4), (r, X), (co,   X)])
	
	push!(Transitions, State_2 => [(me, State_3), (md, State_6),(mw,X), (r, State_2)])
	
	push!(Transitions, State_3 => [(me, State_4), (mw, State_2), (r, State_3)])
	
	push!(Transitions, State_4 => [(md, State_8), (mu, X), (r, State_4)])
	
	push!(Transitions, State_5 => [( mw, State_5), (me, State_6), (r, State_5)])
	
	push!(Transitions, State_6 => [( mw, State_5), (me, State_7), (r, State_6)])
	
	push!(Transitions, State_7 => [( mw, State_6), (me, State_8), (r, State_7)])
	
	Transitions
end


# ╔═╡ a0d29bd7-656f-4c7a-b9a1-286ec45074c6
md"""Concepts of A*

* g  (n) : The actual cost path from the start node to the current node. 
* h ( n) : The actual cost path from the current node to goal node.
* f  (n) : The actual cost path from the start node to the goal node.
* f(n) = g(n)+h(n)
"""


# ╔═╡ 8f1e048a-544c-4247-bd70-f7e46afdf87c
md"#### Define the Heuristic Function"

# ╔═╡ 57c59a7a-cacc-41cc-bab7-51f35d05071c
function getHeuristic(State)	
	total = 0
	for i in 1:length(Action.cost)
		total += Action.cost[i]
	end
  return total+3
end


# ╔═╡ 88a87f58-1181-413e-a291-7a95216bf032
md"#### Define Astar alogrithm"

# ╔═╡ 20602ef4-024b-4dce-aea1-4279b3926802
function Astar(initial, transition_dict, goal_state, all_candidates,
		add_candidate, remove_candidate)
		explored = []
		ancestors = Dict{State, State}()
	     total_cost = 0
		the_candidates = add_candidate(all_candidates, initial, 0)
		parent = initial
	
	
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			 current_state = t1
			 the_candidates = t2
				
			 push!(explored, current_state)
             candidates = transition_dict[current_state]
			 for single_candidate in candidates
			   println(the_candidates)
			
				  if !(single_candidate[2] in explored)
					   push!(ancestors, single_candidate[2] => current_state)
					
					
					if (single_candidate[2] in  goal_state)
							return return_Result(transition_dict, ancestors,
							  initial, single_candidate[2])
					  else
					 
							the_candidates = add_candidate(the_candidates,
							single_candidate[2], single_candidate[1].cost + getHeuristic(single_candidate[2].parcels))
					
					end
				  end
				
			  end
         end
     end
	println(the_candidates)
end


# ╔═╡ 455ab8bc-e07a-4a16-9d6e-97437ae02ea2
# begin
# ALL_CANDIDATES = PriorityQueue{State,Int64}(Base.Order.Reverse)
# result_actions = astar(INITIAL_STATE, MODEL,[GOAL_STATE],ALL_CANDIDATES, addQueue, removeQueue)
# 	with_terminal() do
# 		println(result_actions)
# 	end
# end



# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
Pkg = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╟─113b0c9d-2bc8-4dc0-8b91-39af696fa47d
# ╠═9b206d20-caf3-11ec-0206-bb60a1af7ad5
# ╠═8057b77b-9925-4931-b783-9804e283085b
# ╠═a4f98e83-cb95-4242-9f67-8a387bef8eb9
# ╠═8b3485aa-e839-4272-96af-b7c282336b69
# ╠═52bf76b6-5106-44dc-b212-79cf54663adc
# ╟─0540ab39-fd77-4325-90db-e6c6db15a359
# ╟─d2a3ad4b-c0a1-4f84-a050-5349f8551c20
# ╟─f4e917cd-8352-403e-beb8-5b95ed66aab5
# ╠═a0b9597a-bb37-478d-9943-7e22d894f900
# ╠═7905b5cf-47ec-4fa9-a7b4-099181cda229
# ╠═fc61dbec-0acd-4940-9cff-341f3b0d4191
# ╟─6b5017a7-6c0b-432f-a669-1de5c4f8c5e2
# ╟─b2ace667-ba45-4019-b33d-b8e979f9b747
# ╟─50b7aee0-fe18-43bc-8986-1be8adcc0b47
# ╟─f9771065-d0ba-4687-a141-1870b93b2173
# ╟─10d38fe0-60d1-4ada-bb5d-241eba53f3aa
# ╟─6bcfb880-cc86-4dd2-8769-ee5fc2e23ca7
# ╟─ebbd3aa4-9550-472d-93af-12a369879302
# ╠═923af068-39e6-4ff0-aeb4-6ac1ca433101
# ╠═1a0641ad-d75b-4d95-a574-a68c87a0fd80
# ╠═8928d519-079b-44e7-888d-7ded0c5d7941
# ╠═f787be50-eb2c-48cd-ac34-38f4b552bd0b
# ╠═51282cb4-22f4-4240-b9a1-b3e7f92359fc
# ╠═821f4235-0d9b-429d-9148-09217b15e51e
# ╠═ad934ec1-d89c-4066-b535-cc7e37f2e32f
# ╠═a0d29bd7-656f-4c7a-b9a1-286ec45074c6
# ╟─8f1e048a-544c-4247-bd70-f7e46afdf87c
# ╠═57c59a7a-cacc-41cc-bab7-51f35d05071c
# ╠═88a87f58-1181-413e-a291-7a95216bf032
# ╠═20602ef4-024b-4dce-aea1-4279b3926802
# ╠═455ab8bc-e07a-4a16-9d6e-97437ae02ea2
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
