### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 2e362ea1-5422-4d00-bccd-57d8da7f711f
using Pkg

# ╔═╡ 037bd712-bd25-11ec-0b43-0550050faaa4
using Markdown

# ╔═╡ 239bcee0-c683-458f-ad8e-6c6fddbf8f20
using DataStructures

# ╔═╡ 52aeee80-c5aa-49b0-b1fb-2c34fff0da43
using PlutoUI


# ╔═╡ 8159a06a-f6bf-4fcb-bfca-f6a3c4ec97a8
md"# Assignmet Problem 2" 

# ╔═╡ c595b1a5-e14c-4967-bea1-97b1d69d6374
md"# Constraint Satisfaction Problem" 

# ╔═╡ d007bcbd-db41-4667-b007-63bf393779b1
 @enum Domain f1 f2 f3 f4

# ╔═╡ fc06c45c-578b-4684-a4af-6385929df723
struct Action
	mrt::Int
	ml::Int
	mt::Int
    msr::Float32
end


# ╔═╡ 40f12ae7-0ad7-409c-8620-dd53ca4d2167
struct ServerlessF
    signature::String
	mrt::Int
	ml::Int
	mt::Int
    msr::Float32
end


# ╔═╡ 86aa4f06-4111-4ba7-aac8-d1155d9b8478
mutable struct Variable
name::String
value::Union{Nothing,Domain}
action::Union{Nothing,Action}
prohibit::Vector{Domain}
restriction::Int64
end


# ╔═╡ 0f8eaecb-39df-4d35-9fc2-2d9d836f8e38
struct Value
variables::Vector{Variable}
constraints::Vector{Tuple{Variable,Variable}}
end


# ╔═╡ 0ee2afa0-ea00-4028-9e7e-e6f847d4c137
 a1 = Variable("Action1",nothing,nothing, [], 0)


# ╔═╡ 0a2c21cf-8184-4bad-a89b-44dac531e695
 a2 = Variable("Action2",nothing,nothing, [], 0)

# ╔═╡ 78ff3086-8b7f-4a12-8a51-93c51b359883
 a3= Variable("Action3",nothing,nothing, [], 0)


# ╔═╡ 52da87d3-b6ca-4941-948e-baefc196ab40
 a4= Variable("Action4",nothing,nothing, [], 0)


# ╔═╡ 2bc8dbc5-7257-492f-be5b-2caa4e72db84
begin
f11= ServerlessF("input", 20, 30, 5, 0.5)
f22= ServerlessF("input", 20, 30, 5, 0.4)
f33= ServerlessF("output", 20, 30, 5, 0.2)
f44= ServerlessF("output", 20, 30, 5, 0.1)
end

# ╔═╡ 073197a6-ec0f-4af7-a250-c1b35e828e04
begin
a11= Action( 20, 30, 5, 0.5)
a22= Action( 25, 87, 5, 0.4)
a32= Action( 76, 50, 5, 0.2)
a42= Action( 28, 80, 5, 0.1)
end

# ╔═╡ 2c5c4a3d-9a31-4071-994f-355bd2333c9e
md"# Forward Check and Back Propagation" 

# ╔═╡ a8372d63-1e81-4d5f-aeb6-c913d1b532c7
function solver(a::Value, assign)
	for e in a.variables
		if e.restriction == 4
			return []
		else
		
			next = rand(setdiff(Set([f1,f2,f3,f4]),
			Set(e.prohibit)))
			e.value = next

			for constraint in a.constraints
				if !((constraint[1] == e) || (constraint[2] == e))
					continue
				else
				if constraint[1] == e
				   push!(constraint[2].prohibit, next)
                   constraint[2].restriction += 1



				else
                   push!(constraint[1].prohibit, next)
                   constraint[1].restriction += 1
					
				
                end
           end
     end
				
       push!(assign, e.name => next)
   end
end
return assign
end

# ╔═╡ 4b70f64a-f01d-4338-8fb4-fe30e5156318
md"# ARC CONSISTENCY" 

# ╔═╡ b9f53b8f-4ffa-4f45-8fa4-35eb40501ce9
function AC3(pb::T; queue::Union{Nothing, AbstractVector}=nothing, removals::Union{Nothing, AbstractVector}=nothing) where {T <: Value}
    if (typeof(queue) <: Nothing)
        queue = collect((Xi, Xk) for Xi in pb.vars for Xk in pb.constraints[Xi]);
    end
    support_pruning(pb);
    while (length(queue) != 0)
        local X = popfirst!(queue);   
        local Xi = getindex(X, 1);
        local Xj = getindex(X, 2);
        if (revise(pb, Xi, Xj, removals))
            if (!haskey(pb.vars, Xi))
                return false;
            end
            for Xk in pb.vars[Xi]
                if (Xk != Xi)
                    push!(queue, (Xk, Xi));
                end
            end
        end
    end
    return true;
end		



# ╔═╡ b42a1410-af81-4e65-8ecd-42f40fbcf56c
problem= Value([a1,a2,a3,a4,],[(a1,a2),(a2,a1),(a1,a4)])

# ╔═╡ dc3518c6-e3cd-4016-a1db-ca2c2ed9555d
solver(problem, [])

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
Pkg = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "621f4f3b4977325b9128d5fae7a8b4829a0c2222"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.4"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═8159a06a-f6bf-4fcb-bfca-f6a3c4ec97a8
# ╠═037bd712-bd25-11ec-0b43-0550050faaa4
# ╠═2e362ea1-5422-4d00-bccd-57d8da7f711f
# ╠═239bcee0-c683-458f-ad8e-6c6fddbf8f20
# ╠═52aeee80-c5aa-49b0-b1fb-2c34fff0da43
# ╠═c595b1a5-e14c-4967-bea1-97b1d69d6374
# ╠═d007bcbd-db41-4667-b007-63bf393779b1
# ╠═fc06c45c-578b-4684-a4af-6385929df723
# ╠═40f12ae7-0ad7-409c-8620-dd53ca4d2167
# ╠═86aa4f06-4111-4ba7-aac8-d1155d9b8478
# ╠═0f8eaecb-39df-4d35-9fc2-2d9d836f8e38
# ╠═0ee2afa0-ea00-4028-9e7e-e6f847d4c137
# ╠═0a2c21cf-8184-4bad-a89b-44dac531e695
# ╠═78ff3086-8b7f-4a12-8a51-93c51b359883
# ╠═52da87d3-b6ca-4941-948e-baefc196ab40
# ╠═2bc8dbc5-7257-492f-be5b-2caa4e72db84
# ╠═073197a6-ec0f-4af7-a250-c1b35e828e04
# ╠═2c5c4a3d-9a31-4071-994f-355bd2333c9e
# ╠═a8372d63-1e81-4d5f-aeb6-c913d1b532c7
# ╠═4b70f64a-f01d-4338-8fb4-fe30e5156318
# ╠═b9f53b8f-4ffa-4f45-8fa4-35eb40501ce9
# ╠═b42a1410-af81-4e65-8ecd-42f40fbcf56c
# ╠═dc3518c6-e3cd-4016-a1db-ca2c2ed9555d
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
